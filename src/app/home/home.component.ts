import { SessionService } from './../services/session.service';
import { DictionaryType } from './../models/dictionary-type';
import { Component, OnInit } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';
import { Router } from '@angular/router';
import { BaseComponent } from '../base-component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BaseComponent implements OnInit {

  dictionaryTypeIndexes: number[] = [];
  selectedDictionaryTypeIndex = 0;
  DictionaryType = DictionaryType;

  searchedDictionaryWord = "";

  constructor(router: Router, sessionService: SessionService) {
    super(router,sessionService);
  }

  ngOnInit() {
    this.setDictionaries();
    const dictionaryTypesArray = <string[]>Object.values(DictionaryType);
    this.dictionaryTypeIndexes = dictionaryTypesArray.splice(dictionaryTypesArray.length / 2).map(v => parseInt(v));
  }

  setSelectedDictionaryType(changeEvent: MatRadioChange) {
    this.selectedDictionaryTypeIndex = changeEvent.value;
    this.selectedDictionary = undefined;
  }

}
