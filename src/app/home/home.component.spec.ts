import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { FilterDictionariesPipe } from '../pipes/filter-dictionaries.pipe';
import { SessionService } from '../services/session.service';
import { RouterTestingModule } from '@angular/router/testing';
import { DataManagerService } from '../services/data-manager.service';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeComponent, FilterDictionariesPipe],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [SessionService, DataManagerService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should show title-container', () => {
    expect(fixture.nativeElement.querySelector('.title-container')).toBeTruthy();
  });

  it('should show logo', () => {
    expect(fixture.nativeElement.querySelector('.logo')).toBeTruthy();
    expect(fixture.nativeElement.querySelector('.logo').src).toContain('/assets/images/logo.png');
  });

  it('should show title', () => {
    expect(fixture.nativeElement.querySelector('.title')).toBeTruthy();
    expect(fixture.nativeElement.querySelector('.title').textContent).toBe("dictionary");
  });

  it('should show search', () => {
    expect(fixture.nativeElement.querySelector('.search')).toBeTruthy();
  });

  it('should show search bar', () => {
    expect(fixture.nativeElement.querySelector('.search-bar')).toBeTruthy();
  });

  it('should show search button', () => {
    expect(fixture.nativeElement.querySelector('.search-button')).toBeTruthy();
  });

  it('should show choice search bar', () => {
    expect(fixture.nativeElement.querySelector('.choice-search-bar')).toBeTruthy();
  });

  it('should show types of dictionaries', () => {
    expect(fixture.nativeElement.querySelectorAll('.dictionary-type').length).toBe(3);
  });

  it('should show names of dictionaries', () => {
    inject([SessionService], (sessionService: SessionService) =>
      sessionService.dictionaries.subscribe(() => expect(fixture.nativeElement.querySelectorAll('.dictionary-name').length).toBeGreaterThanOrEqual(1)));
  });

  it('should show title of choice panel', () => {
    expect(fixture.nativeElement.querySelectorAll('.choice-title').length).toBeGreaterThanOrEqual(1);
  });

});
