import { DictionaryType } from "./dictionary-type";

export interface Dictionary {
  region?: string;
  source: string;
  sourceLanguage: LanguageDetail;
  targetLanguage: LanguageDetail;
  type: DictionaryType;
}

export interface LanguageDetail {
  id: string;
  language?: string;
}
