export interface Word {
  id: string,
  language: string,
  lexicalEntries: LexicalEntry[],
  pronunciations: Pronunciation[],
  type: string,
  word: string
}

interface LexicalEntry {
  compounds: BaseItem[],
  derivativeOf: BaseItem[],
  derivatives: BaseItem[],
  entries: Entry[],
  grammaticalFeatures: BaseField[],
  language: string,
  lexicalCategory: BaseField,
  notes: BaseField[],
  phrasalVerbs: BaseItem[],
  phrases: BaseItem[],
  pronunciations: Pronunciation[],
  root: string,
  text: string,
  variantForms: VariantForm[]
}

interface BaseItem {
  domains: BaseField[],
  id: string,
  language: string,
  regions: BaseField[],
  registers: BaseField[],
  text: string
}

interface BaseField {
  id: string,
  text: string,
  type?: string
}

interface Pronunciation {
  audioFile: string,
  dialects: string[],
  phoneticNotation: string,
  phoneticSpelling: string,
  regions: BaseField[],
  registers: BaseField[],
  variantForms?: VariantForm[]
}

interface Entry {
  crossReferenceMarkers: string[],
  crossReferences: BaseField[],
  etymologies: string[],
  grammaticalFeatures: BaseField[],
  homographNumber: string,
  inflections: Inflection[],
  notes: BaseField[],
  pronunciations: Pronunciation[],
  senses: Sense[],
  variantForms: VariantForm[]
}

interface Sense {
  antonyms: BaseItem[],
  constructions: Construction[]
  crossReferenceMarkers: string[],
  crossReferences: BaseField[],
  definitions: string[],
  domainClasses: BaseField[],
  domains: BaseField[],
  etymologies: string[],
  examples: Example[],
  id: string,
  inflections: Inflection[],
  notes: BaseField[],
  pronunciations: Pronunciation[],
  regions: BaseField[],
  registers: BaseField[],
  semanticClasses: BaseField[],
  shortDefinitions: string[],
  subsenses: {}[],
  synonyms: BaseItem[],
  thesaurusLinks: ThesaurusLink[],
  translations?: Translation[],
  variantForms: VariantForm[]
}

interface VariantForm {
  domains: BaseField[],
  notes: BaseField[],
  pronunciations: Pronunciation[],
  regions: BaseField[],
  registers: BaseField[],
  text: string
}

interface Example {
  collocations?: BaseField[],
  crossReferenceMarkers?: string[],
  crossReferences?: BaseField[],
  definitions: string[],
  domains: BaseField[],
  notes: BaseField[],
  regions: BaseField[],
  registers: BaseField[],
  senseIds: string[],
  text: string,
  translations?: Translation[]
}

interface ThesaurusLink {
  entry_id: string,
  sense_id: string
}

interface Construction {
  domains: BaseField[],
  examples: string[][],
  notes: BaseField[],
  regions: BaseField[],
  registers: BaseField[],
  text: string,
  translations?: Translation[]
}

interface Inflection {
  domains: BaseField[],
  grammaticalFeatures: BaseField[],
  inflectedForm: string,
  lexicalCategory: BaseField,
  regions: BaseField[],
  registers: BaseField[],
  pronunciations: Pronunciation[]
}

interface Translation {
  collocations: BaseField[],
  domains: BaseField[],
  grammaticalFeatures: BaseField[],
  language: string,
  notes: BaseField[],
  regions: BaseField[],
  registers: BaseField[],
  text: string,
  toneGroups: ToneGroup[],
  type: string
}

interface ToneGroup {
  tones: Tone[]
}

interface Tone {
  type: string,
  value: string
}
