export enum DictionaryType {
  all,
  monolingual,
  bilingual
}
