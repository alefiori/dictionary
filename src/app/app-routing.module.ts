import { WordDefinitionComponent } from './word-definition/word-definition.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "definition/:sourceLanguageId/:targetLanguageId/:word",
    component: WordDefinitionComponent
  },
  {
    path: "definition/:languageId/:word",
    component: WordDefinitionComponent
  },
  {
    path: "**",
    redirectTo: "",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
