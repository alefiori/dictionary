import { SessionService } from './services/session.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { RemoveDictionaryWordPipe } from './pipes/remove-dictionary-word.pipe';
import { FilterDictionariesPipe } from './pipes/filter-dictionaries.pipe';
import { WordDefinitionComponent } from './word-definition/word-definition.component';
import { MatSelectModule } from '@angular/material/select';
import { DataManagerService } from './services/data-manager.service';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RemoveDictionaryWordPipe,
    FilterDictionariesPipe,
    WordDefinitionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatRadioModule,
    MatButtonModule,
    ScrollingModule,
    MatIconModule,
    HttpClientModule,
    MatSelectModule,
    MatProgressSpinnerModule
  ],
  providers: [DataManagerService, SessionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
