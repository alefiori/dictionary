import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { WordDefinitionComponent } from './word-definition.component';
import { SessionService } from '../services/session.service';
import { Location } from '@angular/common';
import { DataManagerService } from '../services/data-manager.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('WordDefinitionComponent', () => {
  let component: WordDefinitionComponent;
  let fixture: ComponentFixture<WordDefinitionComponent>;
  let location: Location;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WordDefinitionComponent],
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [SessionService, DataManagerService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WordDefinitionComponent);
    component = fixture.componentInstance;
    location = TestBed.inject(Location);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show search bar', () => {
    expect(fixture.nativeElement.querySelector('.search-bar')).toBeTruthy();
  });

  it('should show search button', () => {
    expect(fixture.nativeElement.querySelector('.search-button')).toBeTruthy();
  });

  it('should show dictionary list drop down', () => {
    expect(fixture.nativeElement.querySelector('.dictionaries-drop-down')).toBeTruthy();
  });

  it('should show logo', () => {
    expect(fixture.nativeElement.querySelector('.logo').src).toContain('assets/images/logo.png');
  });

  it('should go to home after click on logo', () => {
    fixture.nativeElement.querySelector('.logo').click();
    expect(location.path()).toBe('');
  });

});
