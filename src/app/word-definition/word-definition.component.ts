import { Word } from './../models/word';
import { SessionService } from './../services/session.service';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataManagerService } from '../services/data-manager.service';
import { BaseComponent } from '../base-component';

@Component({
  selector: 'app-word-definition',
  templateUrl: './word-definition.component.html',
  styleUrls: ['./word-definition.component.scss']
})
export class WordDefinitionComponent extends BaseComponent {

  searchResult?: Word;
  searchedWord = "";
  showWordNotFoundMessage = false;
  private sourceLanguageId?: string;
  private targetLanguageId?: string;
  private regionId?: string;

  constructor(route: ActivatedRoute, sessionService: SessionService, private dataManager: DataManagerService, router: Router) {
    super(router, sessionService);
    route.url.subscribe(url => {
      if (url.length < 3 || url.length > 4)
        return;
      this.sourceLanguageId = url[1].path;
      this.targetLanguageId = url.length === 4 ? url[2].path : "";
      this.searchedWord = url.length === 4 ? url[3].path : url[2].path;
      this.searchResult = undefined;
      if (this.targetLanguageId === "") {
        const dashIndex = this.sourceLanguageId.indexOf('-');
        if (dashIndex != -1) {
          this.regionId = this.sourceLanguageId.slice(dashIndex + 1);
          this.sourceLanguageId = this.sourceLanguageId.slice(0, dashIndex);
        }
        this.targetLanguageId = this.sourceLanguageId;
      }
      if (this.dictionaries.length === 0)
        this.setDictionaries();
      else
        this.retrieveWordInfo();
    });
  }

  setDictionaries() {
    this.sessionService.dictionaries.subscribe(dictionaries => {
      this.dictionaries = dictionaries;
      this.retrieveWordInfo();
    });
  }

  playAudio(trackUrl: string) {
    const audio = new Audio();
    audio.src = trackUrl;
    audio.load();
    audio.play();
  }

  retrieveWordInfo() {
    for (const dictionary of this.dictionaries)
      if (dictionary.sourceLanguage.id === this.sourceLanguageId && dictionary.targetLanguage.id === this.targetLanguageId && this.regionId === dictionary.region) {
        this.selectedDictionary = dictionary;
        break;
      }
    if (!this.selectedDictionary)
      return;
    const sourceLanguageId = this.selectedDictionary.region ? this.selectedDictionary.sourceLanguage.id + `-${this.selectedDictionary.region}` : this.selectedDictionary.sourceLanguage.id;
    const targetLanguageId = this.selectedDictionary.region ? this.selectedDictionary.targetLanguage.id + `-${this.selectedDictionary.region}` : this.selectedDictionary.targetLanguage.id;
    this.dataManager.searchWord(this.searchedWord, sourceLanguageId, targetLanguageId).subscribe(
      result => {
        this.searchResult = result;
        this.showWordNotFoundMessage = false;
      },
      () => this.showWordNotFoundMessage = true);
  }

  openWordLink(word: string) {
    this.searchWord(word, this.sourceLanguageId, this.targetLanguageId, this.regionId);
  }

}
