import { SessionService } from './services/session.service';
import { Router } from '@angular/router';
import { Dictionary } from './models/dictionary';

export abstract class BaseComponent {

  currentWord = "";
  dictionaries: Dictionary[] = [];
  selectedDictionary?: Dictionary;

  constructor(private router: Router, protected sessionService: SessionService) { }

  protected setDictionaries() {
    this.sessionService.dictionaries.subscribe(dictionaries => this.dictionaries = dictionaries);
  }

  selectDictionary(dictionary: Dictionary) {
    this.selectedDictionary = dictionary;
  }

  searchWord(word?: string, sourceLanguageId?: string, targetLanguageId?: string, regionId?: string) {
    word = word ? word : this.currentWord;
    if (word.trim() === "")
      return;
    if (!sourceLanguageId || !targetLanguageId) {
      if (!this.selectedDictionary)
        return;
      sourceLanguageId = this.selectedDictionary.sourceLanguage.id;
      targetLanguageId = this.selectedDictionary.targetLanguage.id;
      regionId = this.selectedDictionary.region;
    }
    const languagesId = regionId ? sourceLanguageId + "-" + regionId : sourceLanguageId + "/" + targetLanguageId;
    this.router.navigateByUrl(`/definition/${languagesId}/${word}`);
  }

  onSearchBarKeyUp(event: any) {
    this.currentWord = event.srcElement.value;
    if (event.key === "Enter")
      this.searchWord();
  }

}
