import { Pipe, PipeTransform } from '@angular/core';
import { DictionaryType } from '../models/dictionary-type';
import { Dictionary } from '../models/dictionary';

@Pipe({
  name: 'filterDictionaries'
})
export class FilterDictionariesPipe implements PipeTransform {

  transform(dictionaries: Dictionary[] | null, dictionaryType?:DictionaryType, searchWord?: string): Dictionary[] {
    if (!dictionaries)
      return [];
    if (dictionaryType)
      dictionaries = dictionaries.filter(dictionary => dictionary.type === dictionaryType);
    if (searchWord)
      dictionaries = dictionaries.filter(dictionary => dictionary.source.toLowerCase().includes(searchWord.toLowerCase()));
    return dictionaries;
  }

}
