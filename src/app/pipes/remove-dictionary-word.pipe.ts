import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removeDictionaryWord'
})
export class RemoveDictionaryWordPipe implements PipeTransform {

  transform(dictionaryName: string): string {
    return dictionaryName.includes("Dictionary") ? dictionaryName.replace("Dictionary", '') : dictionaryName;
  }

}
