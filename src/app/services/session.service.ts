import { Observable, of, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { DataManagerService } from './data-manager.service';
import { Dictionary } from '../models/dictionary';
import { catchError, share, tap } from 'rxjs/operators';

@Injectable()
export class SessionService {

  private _dictionaries: Dictionary[] = [];
  private _dictionariesRetrieval?:Observable<Dictionary[]>;

  constructor(private dataManager: DataManagerService) { }

  get dictionaries(): Observable<Dictionary[]> {
    if (this._dictionaries?.length > 0)
      return of(this._dictionaries);

    if (this._dictionariesRetrieval)
      return this._dictionariesRetrieval;

    this._dictionariesRetrieval = this.dataManager.retrieveDictionariesInfo().pipe(
      tap(dictionaries => {
        this._dictionaries = dictionaries;
        this._dictionariesRetrieval = undefined;
      }),
      catchError(error => {
        this._dictionariesRetrieval = undefined;
        return throwError(error);
      }),
      share()
    );

    return this._dictionariesRetrieval;

  }



}
