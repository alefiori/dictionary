import { Word } from './../models/word';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Dictionary } from '../models/dictionary';
import { DictionaryType } from '../models/dictionary-type';

@Injectable()
export class DataManagerService {

  constructor(private httpClient: HttpClient) { }

  retrieveDictionariesInfo(): Observable<Dictionary[]> {
    return this.httpClient.get('/oxfordapi/languages').pipe(map((response: any) => {
      return response.results.map((result: any) => {
        result.type = DictionaryType[result.type];
        return result;
      }).sort((languageA: Dictionary, languageB: Dictionary) => {
        if (languageA.source < languageB.source)
          return -1;
        if (languageA.source > languageB.source)
          return 1;
        return 0;
      });
    }));
  }

  searchWord(searchWord: string, sourceLanguageId: string, targetLanguageId: string): Observable<Word> {
    const url = sourceLanguageId === targetLanguageId ? `/oxfordapi/entries/${sourceLanguageId}/${searchWord}` : `/oxfordapi/translations/${sourceLanguageId}/${targetLanguageId}/${searchWord}`;
    return this.httpClient.get(url).pipe(map((response: any) => {
      if (response.results)
        return response.results[0];
      throw new Error(response.error);
    }));
  }

}
